#!/usr/bin/env python3
# -*- conding: utf-8 -*-

import os
import requests
import json

class TwitchAPI():
	def __init__(self):
		self.path = "https://api.twitch.tv/helix"

	def get_user(self,user_login):
		return (f"{self.path}/users?login={user_login}","GET")

	def get_stream(self,user_login):
		return (f'{self.path}/streams?user_login={user_login}',"GET")

	def get_subscriptions(self):
		return (f"{self.path}/eventsub/subscriptions","GET")

	def get_token(self,client_id,client_secret):
		return (f'https://id.twitch.tv/oauth2/token?client_id={client_id}&client_secret={client_secret}&grant_type=client_credentials',"POST")

class TwitchUser():
	def __init__(self,id = None, login = None):
		self.id = id
		self.login = login
		self.display_name = ''
		self.online_image_url = ''
		self.offline_image_url = ''
		self.broadcaster_type = ''
		self.description = ''
		self.type = ''
		self.view_count = ''
		self.created_at = ''

class TwitchStream():
	def __init__(self, user_id = None, user_login = None):
		self.user_id = user_id
		self.user_login = user_login
		self.id = ''
		self.title = ''
		self.viewer_count = ''
		self.started_at = ''
		self.thumbnail_url = ''

class Twytch():
	def __init__(self,client_id,client_secret):
		self.client_id = client_id
		self.client_secret = client_secret

		(self.token,self.token_type)=self.get_token()

		self.headers = {
				"Authorization":f"{self.token_type.capitalize()} {self.token}",
				"Client-id":self.client_id
			       }
	def get_token(self):
		(path,mthd) = TwitchAPI().get_token(self.client_id,self.client_secret)
		response = requests.request(url=path,method=mthd).json()
		return (response['access_token'],response['token_type'])

	def get_user(self,user_login: str):
		(path,mthd) = TwitchAPI().get_user(user_login)
		return requests.request(url=path,method=mthd,headers=self.headers).json()['data'][0]

	def get_subscriptions(self):
		(path,mthd) = TwitchAPI().get_subscriptions()
		return requests.request(url=path,method=mthd,headers=self.headers).json()

if __name__ == "__main__":
	pass
