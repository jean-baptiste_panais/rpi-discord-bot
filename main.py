#!/usr/bin/env python3
# -*- conding: utf-8 -*-

import os
import requests
import json
#import flask

from twytch import *
from pyscord import *

with open('./env.json') as file:
	env = json.load(file)

TWITCH_API_CLIENT_ID = env['TWITCH_API_CLIENT_ID']
TWITCH_API_CLIENT_SECRET = env['TWITCH_API_CLIENT_SECRET']
DISCORD_API_BOT_TOKEN = env['DISCORD_API_BOT_TOKEN']
CHANNEL_ID = env['CHANNEL_ID']

Bot = DiscordBot(DISCORD_API_BOT_TOKEN)
twitch = Twytch(TWITCH_API_CLIENT_ID,TWITCH_API_CLIENT_SECRET)

def display_online_streamers():
	with open('streamers.txt') as streamers_file:
		for streamer_login in streamers_file:
			streamer_login = streamer_login.replace("\n","")
			(path,mthd) = TwitchAPI().get_stream(streamer_login)
			response = requests.request(url=path,method=mthd,headers=twitch.headers).json()
			if(response['data']!=[]):
				print(f'{streamer_login} est en ligne !')

if __name__ == "__main__":
	pass
