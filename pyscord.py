#!/usr/bin/env python3
# -*- conding: utf-8 -*-

import os
import requests
import json
from datetime import datetime

class DiscordAPI():
	def __init__(self):
		self.path = "https://discord.com/api/v8"

	def get_current_user(self):
		return (f"{self.path}/users/@me","GET")

	def get_user(self,user_id):
		return (f"{self.path}/users/{user_id}","GET")

	def modify_current_user(self):
		return (f"{self.path}/users/@me","PATCH")

	def get_current_user_guilds(self):
		return  (f"{self.path}/users/@me/guilds","GET")

class Embed():
	def __init__(self, title = "Embed Title", type = "rich", description = "Hello World !", url = None, timestamp = datetime.today().isoformat(), color = None, footer = None, image = None, thumbnail = None, video = None, provider = None, author = None, fields = None):
		self.title = title
		self.type = type
		self.description = description
		self.url = url
		self.timestap = timestamp
		self.color = color
		self.footer = footer
		self.image = image
		self.thumbnail = thumbnail
		self.video = video
		self.provider = provider
		self.author = author
		self.fields = fields

	def set_title(self, title: str):
		self.title = title

	def set_type (self, type: str):
		self.type = type

	def set_description(self, description: str):
		self.description = description

	def to_json():
		return {
			"title":self.title,
			"type":self.type,
			"descriptions":self.description
		}

class DiscordBot():
	def __init__(self,token):
		self.token = token
		self.headers =  {"Authorization":f"Bot {token}"}
		self.json = self.get_current_user()
		self.id = self.json['id']
		self.guilds = self.get_guilds()

	def get_guilds(self):
		(path,mthd) = DiscordAPI().get_current_user_guilds()
		return (requests.request(url=path,method=mthd,headers=self.headers)).json()

	def get_current_user(self):
		(path,mthd) = DiscordAPI().get_current_user()
		return (requests.request(url=path,method=mthd,headers=self.headers)).json()

if __name__ == "__main__":
	pass
